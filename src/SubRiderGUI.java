import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by petrushev on 31.10.2016.
 */
public class SubRiderGUI {
    boolean subScrollListnerWestIsTurnOn = false;
    JScrollBar scrollBar;
    JFrame frame = new JFrame();
    TextHalf textHalfWest = new TextHalf();
    TextHalf textHalfEast = new TextHalf();
    JSplitPane splitPane = new JSplitPane();
    SubScrollListnerWest scrollListnerWest = new SubScrollListnerWest();
    SubScrollListnerEast scrollListnerEast = new SubScrollListnerEast();
    WestCaretListner westCaretListner = new WestCaretListner();
    EastCaretListner eastCaretListner = new EastCaretListner();
    TopMenu topMenu = new TopMenu();
    static JTextField  prefixOfAuFile = new JTextField("audio prefix");
    float difernceBenwenHalf;



    public SubRiderGUI() {

        System.out.println("SubRiderGUI in constructor");
        frame.setSize(400, 400);
        frame.getContentPane().add(BorderLayout.CENTER, splitPane);
        splitPane.setResizeWeight(0.5);
        splitPane.setLeftComponent(textHalfWest.jPanel);
        splitPane.setRightComponent(textHalfEast.jPanel);
        textHalfWest.scrollPane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        textHalfEast.textArea.addCaretListener(eastCaretListner);
        textHalfWest.textArea.addCaretListener(westCaretListner);
        textHalfEast.scrollPane.getViewport().addChangeListener(scrollListnerEast);
        textHalfWest.scrollPane.getViewport().addChangeListener(scrollListnerWest);//понять почему рабаотает в конструкторе
        //но не работает в class TextHalf

        //menu
        frame.getContentPane().add(BorderLayout.NORTH, topMenu.menuBar);
        frame.setVisible(true);
    }

    class TopMenu{
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
            JMenuItem openWest = new JMenuItem("Open Left");
            FileOpenLeftListner fileOpenLeftListner;
            JMenuItem openEast = new JMenuItem("Open Right");
            FileOpenRightListner fileOpenRightListner;

        JMenu editMenu = new JMenu("Edit");
        JMenu optionsMenu = new JMenu("Option");
        JMenuItem paste = new JMenuItem("Paste");
        PasteListner pasteListner;


        JMenu help = new JMenu("Help");
            JMenuItem readme = new JMenuItem("Readme");
            JMenuItem about = new JMenuItem("About");



        JButton insertToFile = new JButton("translate");

        //preficsOfAudionFiles



        TopMenu(){
            menuBar.add(fileMenu);
            //file
                fileMenu.add(openWest);
                fileOpenLeftListner = new FileOpenLeftListner();
                openWest.addActionListener(fileOpenLeftListner);

                fileMenu.add(openEast);
                fileOpenRightListner = new FileOpenRightListner();
                openEast.addActionListener(fileOpenRightListner);
            //menuBar.add(editMenu);
            editMenu.addSeparator();
            editMenu.add(optionsMenu);
            //paste
            //menuBar.add(paste);
            pasteListner = new PasteListner();
            paste.addActionListener(pasteListner);
            //help
            menuBar.add(help);
            help.add(readme);
            help.add(about);
            //insertToFile Button
            menuBar.add(insertToFile);
            insertToFile.addActionListener(pasteListner);

            menuBar.add(prefixOfAuFile);



        }


    }


    class TextHalf {
        JTextArea textArea = new JTextArea(10,30);
        JScrollPane scrollPane = new JScrollPane(textArea);
        JPanel jPanel = new JPanel();
        float position;
        public TextHalf() {
            position = this.scrollPane.getVerticalScrollBar().getValue();
            jPanel.setLayout(new BorderLayout());
            textArea.setEditable(true);
            textArea.setSize(30, 30);
            System.out.println("in TextHalf");
            scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            jPanel.setBackground(Color.BLUE);
            jPanel.add(scrollPane);

        }

        float getPositionProcent(){
            System.out.println("ListnerWest Change");
            float positionProcent;
            try {
                positionProcent =(float)scrollPane.getVerticalScrollBar().getValue()/(((float)scrollPane.getVerticalScrollBar().getMaximum()-(float)scrollPane.getVerticalScrollBar().getHeight())/100);
            }catch (Throwable t){
                positionProcent = 0;
            }
            return positionProcent;
        }

        void setPositionProcent(float positionProcent){

            float mirrowEast;
            try {
                mirrowEast = positionProcent* (((float)scrollPane.getVerticalScrollBar().getMaximum()-(float)scrollPane.getVerticalScrollBar().getHeight())/100);
            }catch (Throwable t){
                mirrowEast = 0;
            }

            System.out.println("MirrowEast: " + mirrowEast);


            scrollBar  = scrollPane.getVerticalScrollBar();
            scrollBar.setValue((int) mirrowEast);
        }

    }

    class SubScrollListnerWest implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (subScrollListnerWestIsTurnOn){return;}
            System.out.println("West scroll" + textHalfWest.getPositionProcent());
            textHalfEast.setPositionProcent(textHalfWest.getPositionProcent() - difernceBenwenHalf);

        }
    }

    class SubScrollListnerEast implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (subScrollListnerWestIsTurnOn){return;}
            difernceBenwenHalf = textHalfWest.getPositionProcent() - textHalfEast.getPositionProcent();
            System.out.println("East scroll" + textHalfEast.getPositionProcent()+" diferent"+ difernceBenwenHalf);

        }
    }

    class WestCaretListner implements CaretListener {
        @Override
        public void caretUpdate(CaretEvent e) {
            String text = textHalfWest.textArea.getSelectedText();
            if (text == null){return;}
            System.out.println("caret listneh West");
            SubTextWorker.setCurrentSelectionWestAndEastNull(text);
        }
    }

    class EastCaretListner implements CaretListener {
        @Override
        public void caretUpdate(CaretEvent e) {
            String text = textHalfEast.textArea.getSelectedText();
            if (text == null){return;}
            System.out.println("caret listneh East");
            SubTextWorker.setCurrentSelectionEast(text);

            if (prefixOfAuFile.getText().equals("audio prefix")){
                prefixOfAuFile.setText(String.valueOf(System.currentTimeMillis()));
            }
            try {
                SubTextWorker.sendTextToAnkiFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }


    class DelayTurnOffScroll implements Runnable{

        @Override
        public void run() {
            subScrollListnerWestIsTurnOn = true;

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            textHalfWest.setPositionProcent(0);
            textHalfEast.setPositionProcent(0);
            difernceBenwenHalf = 0;
            subScrollListnerWestIsTurnOn = false;
        }
    }

    private void bothPositionToZerro() {
        System.out.println("bothPositionToZerroStart");
        DelayTurnOffScroll delayTurnOffScroll = new DelayTurnOffScroll();
        Thread delaySubScrollListnerWestIsTurnOn = new Thread(delayTurnOffScroll);
        delaySubScrollListnerWestIsTurnOn.start();
        System.out.println("bothPositionToZerroStop");


    }

    class FileOpenLeftListner implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("FileOpenLeftListner");
            try {
                fileChoser(textHalfWest);

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            bothPositionToZerro();
        }
    }
    class FileOpenRightListner implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("FileOpenRightListner");
            try {
                fileChoser(textHalfEast);

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            bothPositionToZerro();
        }
    }

    private void fileChoser (TextHalf textHalf) throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        int ret = fileChooser.showDialog(null, "Открыть файл");

        if (ret == JFileChooser.APPROVE_OPTION) {
            //File file = fileopen.getSelectedFile();
            System.out.println("file is opend ");
            System.out.println(fileChooser.getSelectedFile());
            String fileContent = readFile(fileChooser.getSelectedFile().toString());
            textHalf.textArea.append(fileContent);

        }else {
            System.out.println("file is not chosen or right");
        }
    }

    private String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader (file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

    class PasteListner implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("pasteListner");
            String text = textHalfWest.textArea.getSelectedText();
            if (text == null){return;}
            try {
                textHalfWest.textArea.replaceSelection(text +" ("+ Translator.callUrlAndParseResult(text)+") " );
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }
    }

    public static String getAudioPrefix(){
        return prefixOfAuFile.getText();
    }

}
