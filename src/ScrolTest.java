import javax.swing.*;
import java.awt.*;

/**
 * Created by petrushev on 02.11.2016.
 */
public class ScrolTest {

    JFrame frame = new JFrame();
    JPanel panel = new JPanel();
    JTextArea textArea = new JTextArea(1,50);
    JScrollPane scrollPane = new JScrollPane(textArea);

    public static void main(String[] args) {

        ScrolTest scrolTest = new ScrolTest();
        scrolTest.go();
    }
    void go (){
        panel.setLayout(new BorderLayout());
        textArea.setEditable(true);
        panel.add(textArea);
        frame.setSize(300, 300);
        frame.setVisible(true);
        frame.getContentPane().add(BorderLayout.CENTER, panel);
        panel.setBackground(Color.CYAN);
    }

}
