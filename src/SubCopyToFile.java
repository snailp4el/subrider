
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;


public class SubCopyToFile {







    static void CopyStringToFile(String s) throws IOException {

        List<String> lines;
        Path file = Paths.get("Anki.txt");

        //check file
        File f = new File(file.toAbsolutePath().toString());
        if(!f.exists()) {
            System.out.println("file "+ f +" dont exist");
            lines = Arrays.asList();
            Files.write(file, lines, Charset.forName("UTF-8"));
        }
        lines = Arrays.asList(s);

        Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);

    }

}