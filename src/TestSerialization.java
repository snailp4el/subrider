import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class TestSerialization {

    JFrame jf = new JFrame();
    JTextField tf = new JTextField("qqqq");
    JButton bSave = new JButton("save");
    JButton bLoad = new JButton("load");
    Save save = new Save();
    Load load = new Load();

    FileInputStream fis;
    ObjectInputStream ois;

    public static void main(String[] args) {
        System.out.println("start");
        TestSerialization testSerialization = new TestSerialization();
        testSerialization.go();
    }

    private void go (){

        try {
            fis = new FileInputStream("testSerialization.txt");
            ois = new ObjectInputStream(fis);
            tf = (JTextField) ois.readObject();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        jf.setSize(300, 300);
        bSave.addActionListener(save);
        bLoad.addActionListener(load);
        jf.getContentPane().add(BorderLayout.CENTER, tf);
        jf.getContentPane().add(BorderLayout.WEST, bSave);
        jf.getContentPane().add(BorderLayout.EAST, bLoad);
        jf.setVisible(true);



    }

    class Save implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("save");
            try {
                FileOutputStream fos = new FileOutputStream("testSerialization.txt");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(tf);
                oos.flush();
                oos.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }


        }
    }

    class Load implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("load");

            try {
                fis = new FileInputStream("testSerialization.txt");
                ois = new ObjectInputStream(fis);
                tf = (JTextField) ois.readObject();
            } catch (IOException | ClassNotFoundException e1) {
                e1.printStackTrace();
            }

            System.out.println(tf.getText());

        }
    }



}
