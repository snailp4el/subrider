import java.awt.datatransfer.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class CliboardString implements Transferable, ClipboardOwner {

    private final DataFlavor flavor;

    private final String data;
    private final String javaEncoding;

    public CliboardString(String data,String flavorEncoding,String javaEncoding) {
        this.data = data;
        this.javaEncoding = javaEncoding;
        flavor =  new DataFlavor("text/plain;charset="+flavorEncoding, "TEXT");
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] {flavor};
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor!=null && flavor.equals(this.flavor);
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (isDataFlavorSupported(flavor)) {
            return new ByteArrayInputStream(data.getBytes(javaEncoding));
        }
        throw new UnsupportedFlavorException(flavor);
    }

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }

}