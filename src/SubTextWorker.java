
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

public class SubTextWorker {

    private static String currentSelectionWest;
    private static String currentSelectionEast;
    private static int currentNumberOfPastToAnki = 1;


    public static void setCurrentSelectionWestAndEastNull(String currentSelectionWest) {
        currentSelectionWest = normalizeString(currentSelectionWest);
        SubTextWorker.currentSelectionWest = currentSelectionWest;
        SubTextWorker.currentSelectionEast = null;
        SystemClipboard.copy(currentSelectionWest);
    }

    public static void setCurrentSelectionEast(String currentSelectionEast) {
        currentSelectionEast = normalizeString(currentSelectionEast);
        SubTextWorker.currentSelectionEast = currentSelectionEast;
        SystemClipboard.copy(currentSelectionEast);
    }

    public static void sendTextToAnkiFile() throws IOException {
        if (currentSelectionEast != null && currentSelectionWest != null){
            String toBuffer = SubRiderGUI.getAudioPrefix() +"_"+ currentNumberOfPastToAnki++ ;
            String toFile = currentSelectionWest+ "=" +currentSelectionEast + "=" +"[sound:" +toBuffer +".mp3]";
            toFile = toFile.replaceAll("(\\r)", "");
            SubCopyToFile.CopyStringToFile(toFile);
            SystemClipboard.copy(toBuffer);
            currentSelectionWest = null;
            currentSelectionEast = null;
        }else {
            System.out.println("sendTextToAnkiFile empty");
        }
    }

    private static String normalizeString(String selectedString){

        String sss = null;
        System.out.printf("Caret - ");
        if(selectedString != null){
            //selectedString = selectedString.replaceAll("[<i>]", "");
            //selectedString = selectedString.replaceAll("[</i>]", "");
            selectedString = selectedString.replaceAll("[=]", "");
            selectedString = selectedString.replaceAll("[0-9]", "");
            selectedString = selectedString.replaceAll("null", "");
            String lines[] = selectedString.split("\\r?\\n");

            String toOneLine = new String();
            for(String ss:lines){
                if (!ss.contains("-->")||!ss.contains("")){
                    toOneLine = toOneLine + ss +" ";
                }

            }


            sss = new String(toOneLine.getBytes(Charset.forName("utf-8")));
            sss = sss.replace("  ", " ");
            sss = sss.replace("  ", " ");
            sss = sss.replace("  ", " ");
            sss = delLastSpace(sss);
            sss = delFirstSpace(sss);
            System.out.println("before clipboard - " + sss);

        } else {
            System.out.println("StringIsEmphty");
        }

        return sss;

    }

    public static void writeStringToFile(String s) throws FileNotFoundException {


    }

    private static String delLastSpace(String str) {


            if (str != null && str.length() > 0 && str.charAt(str.length()-1)==' ') {
                str = str.substring(0, str.length()-1);
            }
        return str;
    }

    private static String delFirstSpace(String str) {
        if (str != null && str.length() > 0 && str.charAt(0)==' ') {
            System.out.println("in delete first space");
            str = str.substring(1);
        }
        return str;
    }

}
